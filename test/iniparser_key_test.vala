/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */

using IniParser;

class IniParserKeyTest {
  public static int main (string[] args) {
    Test.init (ref args);

    IniParserKeyTest.add_tests ();

    Test.run ();

    return 0;
  }

	public static void add_tests () {
		Test.add_func ( "/iniparser/key", () => {
		  Key test_key = new Key ( "key", "value", KeyWithValue.YES, Commented.NO );

		  assert ( test_key is Key );
		} );

		Test.add_func ( "/iniparser/key/key_with_value", () => {
		  Key test_key = new Key ( "key", "value", KeyWithValue.YES, Commented.NO );

		  string expected_string = "key = value\n";

		  assert ( test_key.to_string () == expected_string );
		} );

		Test.add_func ( "/iniparser/key/key_with_value_commented", () => {
		  Key test_key = new Key ( "key", "value", KeyWithValue.YES, Commented.YES );

		  string expected_string = "# key = value\n";

		  assert ( test_key.to_string () == expected_string );
		} );

		Test.add_func ( "/iniparser/key/key_without_value", () => {
		  Key test_key = new Key ( "key", "", KeyWithValue.NO, Commented.NO );

		  string expected_string = "key\n";

		  assert ( test_key.to_string () == expected_string );
		} );

		Test.add_func ( "/iniparser/key/key_without_value_commented", () => {
		  Key test_key = new Key ( "key", "", KeyWithValue.NO, Commented.YES );

		  string expected_string = "# key\n";

		  assert ( test_key.to_string () == expected_string );
		} );
	}
}
