/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */

using IniParser;

class IniParserLineTest {
  public static int main (string[] args) {
    Test.init (ref args);

    IniParserLineTest.add_tests ();

    Test.run ();

    return 0;
  }

	public static void add_tests () {
		Test.add_func ( "/iniparser/line", () => {
		  Line test_line = new Line ();

		  assert ( test_line is Line );
		} );

		Test.add_func ( "/iniparser/line/string", () => {
		  Line test_line = new Line ();

		  string expected_string = "\n";

		  assert ( test_line.to_string () == expected_string );
		} );
	}
}
