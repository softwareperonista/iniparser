/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */

using IniParser;

class IniParserSectionTest {
  public static int main ( string[] args ) {
    Test.init ( ref args );

    IniParserSectionTest.add_tests ();

    Test.run ();

    return 0;
  }

  public static void add_tests () {
    Test.add_func ( "/iniparser/section", () => {
      Section test_section = new Section ( "section" );

      assert ( test_section is Section );
    } );

    Test.add_func ( "/iniparser/section/string", () => {
      Section test_section = new Section ( "section" );

      string expected_string = "[section]\n";

      assert ( test_section.to_string () == expected_string );
    } );

    Test.add_func ( "/iniparser/section/string_commented", () => {
      Section test_section = new Section ( "section" );

      test_section.set_commented ( Commented.YES );

      string expected_string = "# [section]\n";

      assert ( test_section.to_string () == expected_string );
    } );

    Test.add_func ( "/iniparser/section/no_string", () => {
      Section test_section = new Section ( "" );

      string expected_string = "";

      assert ( test_section.to_string () == expected_string );
    } );

    Test.add_func ( "/iniparser/section/no_string_with_key", () => {
      Section test_section = new Section ( "" );

      Key test_key = new Key ( "key", "value", KeyWithValue.YES, Commented.NO );

      test_section.add_key ( test_key );

      string expected_string = "key = value\n";

      assert ( test_section.to_string () == expected_string );
    } );

    Test.add_func ( "/iniparser/section/with_key", () => {
      Section test_section = new Section ( "section" );

      Key test_key = new Key ( "key", "value", KeyWithValue.YES, Commented.NO );

      test_section.add_key ( test_key );

      string expected_string = "[section]\nkey = value\n";

      assert ( test_section.to_string () == expected_string );
    } );

    Test.add_func ( "/iniparser/section/with_key_section_commented", () => {
      Section test_section = new Section ( "section" );

      test_section.set_commented ( Commented.YES );

      Key test_key = new Key ( "key", "value", KeyWithValue.YES, Commented.NO );

      test_section.add_key ( test_key );

      string expected_string = "# [section]\n# key = value\n";

      assert ( test_section.to_string () == expected_string );
    } );

    Test.add_func ( "/iniparser/section/with_key_commented", () => {
      Section test_section = new Section ( "section" );

      Key test_key = new Key ( "key", "value", KeyWithValue.YES, Commented.YES );

      test_section.add_key ( test_key );

      string expected_string = "[section]\n# key = value\n";

      assert ( test_section.to_string () == expected_string );
    } );

    Test.add_func ( "/iniparser/section/set_keys", () => {
      Section test_section = new Section ( "section" );

      Key test_key = new Key ( "key", "value", KeyWithValue.YES, Commented.NO );

      test_section.add_key ( test_key );

      Key test_other_key = new Key ( "other_key", "other_value", KeyWithValue.YES, Commented.NO );
      Key test_some_key = new Key ( "some_key", "some_value", KeyWithValue.YES, Commented.YES );

      Array<Key> new_keys = new Array<Key> ();

      new_keys.append_val ( test_other_key );
      new_keys.append_val ( test_some_key );

      test_section.set_keys ( new_keys );

      string expected_string = "[section]\nother_key = other_value\n# some_key = some_value\n";

      assert ( test_section.to_string () == expected_string );
    } );

    Test.add_func ( "/iniparser/section/set_keys_commented", () => {
      Section test_section = new Section ( "section" );

      test_section.set_commented ( Commented.YES );

      Key test_key = new Key ( "key", "value", KeyWithValue.YES, Commented.NO );

      test_section.add_key ( test_key );

      Key test_other_key = new Key ( "other_key", "other_value", KeyWithValue.YES, Commented.NO );
      Key test_some_key = new Key ( "some_key", "some_value", KeyWithValue.YES, Commented.YES );

      Array<Key> new_keys = new Array<Key> ();

      new_keys.append_val ( test_other_key );
      new_keys.append_val ( test_some_key );

      test_section.set_keys ( new_keys );

      string expected_string = "# [section]\n# other_key = other_value\n# some_key = some_value\n";

      assert ( test_section.to_string () == expected_string );
    } );

    Test.add_func ( "/iniparser/section/replace_key", () => {
      Section test_section = new Section ( "section" );

      Key test_key = new Key ( "key", "value", KeyWithValue.YES, Commented.YES );

      test_section.add_key ( test_key );

      Key test_key_other_value = new Key ( "key", "other_value", KeyWithValue.YES, Commented.NO );

      test_section.replace_key ( test_key_other_value );

      string expected_string = "[section]\nkey = other_value\n";

      assert ( test_section.to_string () == expected_string );
    } );

    Test.add_func ( "/iniparser/section/replace_key_commented", () => {
      Section test_section = new Section ( "section" );

      test_section.set_commented ( Commented.YES );

      Key test_key = new Key ( "key", "value", KeyWithValue.YES, Commented.YES );

      test_section.add_key ( test_key );

      Key test_key_other_value = new Key ( "key", "other_value", KeyWithValue.YES, Commented.NO );

      test_section.replace_key ( test_key_other_value );

      string expected_string = "# [section]\n# key = other_value\n";

      assert ( test_section.to_string () == expected_string );
    } );
  }
}
