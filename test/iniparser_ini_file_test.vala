/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */

using IniParser;

class IniParserFileTest {
  public static int main ( string[] args ) {
    Test.init ( ref args );

    IniParserFileTest.add_tests ();

    string expected_string = "# comment\n\n[options]\n#key = value\nkey2 = value2\noption\n\n" +
                             "[repo1]\nrepo1_key = value\n\n" +
                             "#[repo2]\n# repo2_key = value\n#repo2_key2 = value2\n" +
                             "# [repo3]\n# repo3_key = value\n# repo3_key3 = value3";

    try {
      FileUtils.set_contents ( "ini_file_test.conf", expected_string );
    } catch ( Error e ) {
      stderr.printf ( "Error creating ini file (%s)\n", e.message );
    }

    string deskto_file = "[Desktop Entry]\n" +
                         "Name[es]=Pace\n" +
                         "Name=Pace\n" +
                         "Exec=pace\n" +
                         "Terminal=false\n" +
                         "Type=Application\n" +
                         "Categories=GTK;System;\n" +
                         "Icon[es]=ar.com.softwareperonista.Pace\n" +
                         "Icon=ar.com.softwareperonista.Pace\n" +
                         "StartupNotify=true";

    try {
      FileUtils.set_contents ( "file_test.desktop", deskto_file );
    } catch ( Error e ) {
      stderr.printf ( "Error creating desktop file (%s)\n", e.message );
    }

    Test.run ();

    return 0;
  }

  public static void add_tests () {
    Test.add_func ( "/iniparser/ini_file", () => {
      IniFile test_file = new IniFile ( "ini_file_test.conf" );

      assert ( test_file is IniFile );
    } );

    Test.add_func ( "/iniparser/ini_file/bad_file", () => {
      IniFile test_file = new IniFile ( "no_existent.conf" );

      assert ( test_file is IniFile );
    } );

    Test.add_func ( "/iniparser/ini_file/string", () => {
      string expected_string = "# comment\n\n[options]\n# key = value\nkey2 = value2\noption\n\n" +
                               "[repo1]\nrepo1_key = value\n\n" +
                               "# [repo2]\n# repo2_key = value\n# repo2_key2 = value2\n" +
                               "# [repo3]\n# repo3_key = value\n# repo3_key3 = value3\n";

      IniFile test_file = new IniFile ( "ini_file_test.conf" );

      assert ( test_file.to_string () == expected_string );
    } );

    Test.add_func ( "/iniparser/ini_file/get_sections", () => {


      IniFile test_file = new IniFile ( "ini_file_test.conf" );

      assert ( test_file.get_sections ().length == 4 );
    } );

    Test.add_func ( "/iniparser/ini_file/set_sections", () => {
      string expected_string = "# comment\n\n[options]\n# key = value\nkey2 = value2\noption\n\n" +
                               "[repo1]\nrepo1_key = value\n\n" +
                               "# [repo3]\n# repo3_key = value\n# repo3_key3 = value3\n";

      IniFile test_file = new IniFile ( "ini_file_test.conf" );

      var sections = test_file.get_sections ();
      sections.remove_index_fast ( 2 );
      test_file.set_sections ( sections );

      assert ( test_file.to_string () == expected_string );
    } );

    Test.add_func ( "/iniparser/ini_file/get_section", () => {


      IniFile test_file = new IniFile ( "ini_file_test.conf" );
      Section section = test_file.get_section ( "repo1" );

      assert ( section.to_string () == "[repo1]\nrepo1_key = value\n" );
    } );

    Test.add_func ( "/iniparser/ini_file/get_section_non_existent", () => {


      IniFile test_file = new IniFile ( "ini_file_test.conf" );
      Section section = test_file.get_section ( "non_existent" );

      assert ( section == null );
    } );

    Test.add_func ( "/iniparser/ini_file/get_path", () => {


      IniFile test_file = new IniFile ( "ini_file_test.conf" );

      assert ( test_file.get_path () == "ini_file_test.conf" );
    } );

    Test.add_func ( "/iniparser/ini_file/desktopfile", () => {
      string expected_string = "[Desktop Entry]\n" +
                               "Name[es] = Pace\n" +
                               "Name = Pace\n" +
                               "Exec = pace\n" +
                               "Terminal = false\n" +
                               "Type = Application\n" +
                               "Categories = GTK;System;\n" +
                               "Icon[es] = ar.com.softwareperonista.Pace\n" +
                               "Icon = ar.com.softwareperonista.Pace\n" +
                               "StartupNotify = true\n";

      IniFile test_file = new IniFile ( "file_test.desktop" );

      assert ( test_file.to_string () == expected_string );
    } );
  }
}
