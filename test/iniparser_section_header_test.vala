/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */

using IniParser;

class IniParserSectionHeaderTest {
  public static int main ( string[] args ) {
    Test.init ( ref args );

    IniParserSectionHeaderTest.add_tests ();

    Test.run ();

    return 0;
  }

  public static void add_tests () {
    Test.add_func ( "/iniparser/section_header", () => {
      SectionHeader test_section = new SectionHeader ( "section_header" );

      assert ( test_section is SectionHeader );
    } );

    Test.add_func ( "/iniparser/section_header/string", () => {
      SectionHeader test_section = new SectionHeader ( "section_header" );

      string expected_string = "[section_header]\n";

      assert ( test_section.to_string () == expected_string );
    } );

    Test.add_func ( "/iniparser/section_header/no-string", () => {
      SectionHeader test_section = new SectionHeader ( "" );

      string expected_string = "";

      assert ( test_section.to_string () == expected_string );
    } );
  }
}
