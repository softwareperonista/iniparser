/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */

using IniParser;

class IniParserCommentTest {
  public static int main (string[] args) {
    Test.init (ref args);

    IniParserCommentTest.add_tests ();

    Test.run ();

    return 0;
  }

	public static void add_tests () {
		Test.add_func ( "/iniparser/comment", () => {
		  Comment test_comment = new Comment ( "comment" );

		  assert ( test_comment is Comment );
		} );

		Test.add_func ( "/iniparser/comment/string", () => {
		  Comment test_comment = new Comment ( "comment" );

		  string expected_string = "# comment\n";

		  assert ( test_comment.to_string () == expected_string );
		} );
	}
}
