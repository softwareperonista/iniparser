/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */

namespace IniParser {
  public class Comment : Line {
    private string text;
    public Comment ( string text ) {
      this.text = text;
    }

    public string get_text () {
      return this.text;
    }

    public void set_text ( string text ) {
      this.text = text;
    }

    public override string to_string () {
      return "# " + this.text + base.to_string ();
    }
  }
}
