/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */

namespace IniParser {
  public class Section : Line {
    private SectionHeader name;
    private Commented commented;
    private Array<Key> keys;
    private int line;

    public Section ( string name ) {
      this.keys = new Array<Key> ();

      this.name = new SectionHeader ( name );
      this.commented = Commented.NO;
    }

    public void add_key ( Key key ) {
      if ( this.commented == Commented.YES ) {
        key.set_commented ( Commented.YES );
      }

      this.keys.append_val ( key );
    }

    public Array<Key> get_keys () {
      return this.keys;
    }

    public void set_keys ( Array<Key> keys ){
      this.keys = new Array<Key> ();

      for ( int i = 0; i < keys.length; i++ ) {
        this.add_key ( keys.index ( i ) );
      }
    }

    public string get_name () {
      return this.name.get_name ();
    }

    public void set_name ( string name ) {
      this.name.set_name ( name );
    }

    public int get_line () {
      return this.line;
    }

    public void set_line ( int line ) {
      this.line = line;
    }

    public Commented get_commented () {
      return this.commented;
    }

    public void set_commented ( Commented commented ) {
      this.commented = commented;

      for ( int i=0; i < this.keys.length; i++ ) {
        this.keys.index ( i ).set_commented ( this.commented );
      }
    }

    public void replace_key ( Key key ){
      for ( int i = 0; i < this.keys.length; i++ ) {
        if ( this.keys.index ( i ).get_key () == key.get_key () ) {
          if ( this.commented == Commented.YES ) {
            key.set_commented ( Commented.YES );
          }

          this.keys.remove_index ( i );
          this.keys.insert_val ( i, key );
        }
      }
    }

    public int quantity_of_lines () {
      return (int)this.keys.length + 1;
    }

    public override string to_string () {
      string text = "";

      if ( this.commented == Commented.YES ) {
        text += "# ";
      }

      text += this.name.to_string ();

      for ( int i=0; i < this.keys.length; i++ ) {
        var key = this.keys.index ( i );

        text += key.to_string ();
      }

      return text;
    }
  }
}
