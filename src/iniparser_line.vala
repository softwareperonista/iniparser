/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */

namespace IniParser {
  public class Line : Object {
    public Line () {}

    public virtual string to_string () {
      return "\n";
    }
  }
}
