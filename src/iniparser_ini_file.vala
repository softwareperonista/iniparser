/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */

namespace IniParser {
  public class IniFile : Object {
    string[] config_data;
    Array<Section> sections;
    Array<Line> lines;
    Section no_section;
    string path;

    public IniFile ( string path ) {
      this.sections = new Array<Section> ();
      this.lines = new Array<Line> ();
      this.path = path;
      this.no_section = new Section ( "" );
      this.lines.append_val ( this.no_section );

      string data = this.load_data ();
      if ( data != null ) {
        this.config_data = data.split ( "\n", 0 );

        this.parse_data ();
      }
    }

    public IniFile.no_comments ( string path ) {
      this.sections = new Array<Section> ();
      this.lines = new Array<Line> ();
      this.path = path;
      this.no_section = new Section ( "" );
      this.lines.append_val ( this.no_section );

      string data = this.load_data ();
      if ( data != null ) {
        this.config_data = data.split ( "\n", 0 );

        this.parse_data ( false );
      }
    }

    private string load_data () {
      string ret = "";

      try {
        FileUtils.get_contents ( this.path, out ret );
      } catch ( Error e ) {
        debug ( "Error opening ini file (%s)\n", e.message );
      }

      return ret;
    }

    private void parse_data ( bool parse_comments = true ) {
      bool commented = false;
      for ( int i=0; i < this.config_data.length; i++ ) {
        string line = this.config_data[i];

        if ( line.get_char ( 0 ) == '#' ) {
          commented = true;
          line = line.substring ( 1 ).strip ();
          if ( !parse_comments ) {
            if (!(line.contains ("Server = "))) {
              continue;
             }
          }
        } else {
          commented = false;
        }

        if ( line.get_char ( 0 ) == '[' && line.contains ( "]" ) ) {
          this.parse_section_header ( line, commented, i );
        } else {
          if ( line.contains ( "=" ) ) {
            this.parse_key ( line, commented );
          } else {
            if ( line.split ( " ", 0 ).length == 1 ) {
              this.parse_option ( line, commented );
            } else {
              if ( commented ) {
                this.parse_comment ( line );
              } else {
                this.parse_line ( line );
              }
            }
          }
        }
      }
    }

    private void parse_section_header ( string line, bool commented, int line_number ) {
      string name = line.substring ( 1, line.index_of ( "]" ) -1 );

      var new_section = new Section ( name );
      if ( commented ) {
        new_section.set_commented ( Commented.YES );
      }

      new_section.set_line ( line_number );

      this.sections.append_val ( new_section );
      this.lines.append_val ( new_section );
    }

    private void parse_key ( string line, bool commented ) {
      var key_value = line.split ( "=", 2 );

      Commented commented_val = Commented.NO;
      if ( commented ) {
        commented_val = Commented.YES;
      }

      this.add_key ( key_value[0].strip (), key_value[1].strip (), KeyWithValue.YES, commented_val );
    }

    private void parse_option ( string line, bool commented ) {
      var key_value = line.split ( " ", 1 );

      Commented commented_val = Commented.NO;
      if ( commented ) {
        commented_val = Commented.YES;
      }

      this.add_key ( key_value[0], "", KeyWithValue.NO, commented_val );
    }

    private void add_key ( string key_name, string key_value, KeyWithValue kwv, Commented commented ) {
      var key = new Key ( key_name, key_value, kwv, commented );

      if ( this.sections.length != 0 ) {
        var last_section = this.sections.index ( this.sections.length-1 );
        last_section.add_key ( key );
      } else {
        this.no_section.add_key ( key );
      }
    }

    private void parse_comment ( string line ) {
      var comment = new Comment ( line.substring ( 0 ) );
      this.lines.append_val ( comment );
    }

    private void parse_line ( string line ) {
      var empty_line = new Line ();
      this.lines.append_val ( empty_line );
    }

    public Array<Key> get_no_section_keys () {
      return this.no_section.get_keys ();
    }

    public string get_path () {
      return this.path;
    }

    public Section? get_section ( string name ) {
      Section ret = null;

      for ( int i = 0; i < this.sections.length; i++ ) {
        if ( this.sections.index ( i ).get_name () == name ) {
          ret = this.sections.index ( i );
        }
      }

      return ret;
    }

    public Array<Section> get_sections () {
      return this.sections;
    }

    public void set_sections ( Array<Section> sections ) {
      int first_section_line = this.sections.index ( 0 ).get_line ();

      this.lines.set_size ( first_section_line );

      for ( int i = 0; i < sections.length; i++ ) {
        this.lines.append_val ( sections.index ( i ) );
        var empty_line = new Line ();
        if ( i < ( sections.length - 1 ) ) {
          this.lines.append_val ( empty_line );
        }
      }

      this.sections = sections;
    }

    public void set_no_section_keys ( Array<Key> keys ) {
      this.no_section.set_keys ( keys );
    }

    public string to_string () {
      string text = "";
      for ( int i = 0; i < this.lines.length; i++ ) {
        text += this.lines.index ( i ).to_string ();
      }

      return text;
    }
  }
}
