/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */

namespace IniParser {
  public class Key : Line {
    private string key;
    private string value;
    private Commented commented;
    private KeyWithValue with_value;

    public Key ( string key, string value, KeyWithValue with_value, Commented commented = Commented.NO ) {
      this.key = key;
      this.value = value;
      this.commented = commented;
      this.with_value = with_value;
    }

    public string get_key () {
      return this.key;
    }

    public void set_key ( string key ) {
      this.key = key;
    }

    public string get_value () {
      return this.value;
    }

    public void set_value ( string value ) {
      this.value = value;
    }

    public Commented get_commented () {
      return this.commented;
    }

    public void set_commented ( Commented commented ) {
      this.commented = commented;
    }

    public KeyWithValue get_with_value () {
      return this.with_value;
    }

    public void set_with_value ( KeyWithValue with_value ) {
      this.with_value = with_value;
    }

    public override string to_string () {
      string line = "";

      if ( this.commented == Commented.YES ) {
        line += "# ";
      }

      line += this.key;
      if ( this.with_value == KeyWithValue.YES ) {
        line += " = ";
        line += this.value;
      }

      return line + base.to_string ();
    }
  }
}
