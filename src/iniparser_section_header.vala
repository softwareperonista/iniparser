/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */

namespace IniParser {
  public class SectionHeader : Line {
    private string name;
    public SectionHeader ( string name ) {
      this.name = name;
    }

    public string get_name () {
      return this.name;
    }

    public void set_name ( string name ) {
      this.name = name;
    }

    public override string to_string () {
      string ret = "";

      if( this.name != "" ) {
        ret = "[" + this.name + "]" + base.to_string ();
      }

      return ret;
    }
  }
}
